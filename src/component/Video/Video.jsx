import React from "react";
import ReactPlayer from 'react-player';

const Video = React.memo(() =>
{
    return (
        <div style={{position: 'relative', width: '100%', height: '100%'}}>
            <ReactPlayer
                className='react-player'
                url='https://www.youtube.com/watch?v=3x-gBvFRn4I'
                width='100%'
                height='100%'
                preload
            />
        </div>
    );
});

export default Video;
