import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import React from "react";

const Info = React.memo(() =>
{
    return (<Grid container>
        <Typography variant="h5">
            INFO
        </Typography>
    </Grid>);
});

export default Info;
