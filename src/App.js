import Button from "@material-ui/core/Button";
import {yellow} from "@material-ui/core/colors";
import Grid from "@material-ui/core/Grid";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import {VideocamTwoTone, PanoramaTwoTone, RefreshTwoTone, InfoTwoTone} from "@material-ui/icons";
import Gallery from "component/Gallery/Gallery";
import Info from "component/Info/Info";
import Video from "component/Video/Video";
import React from 'react';

const ColorButton = withStyles(theme => ({
    root: {
        '&:hover': {
            backgroundColor: '#f8ecc7',
        },
    },
    contained: {
        color: theme.palette.getContrastText(yellow[500]),
        backgroundColor: '#f8ecc7',
    },
}))(Button);

function App()
{
    const [activeButton, setActiveButton] = React.useState('Photo');

    return (
        <div className='container flex-center-col'>
            <Typography variant="h5" style={{marginBottom: 10, marginTop: -10}}>
                OUR GALLERY
            </Typography>
            <Typography variant="h2">
                Layouts Plan
            </Typography>
            <Grid container style={{marginTop: 105}} justify='space-evenly'>
                <Grid item style={{minHeight: 630, width: '40%'}}>
                    {activeButton === 'Info' && <Info/>}
                    {activeButton === 'Photo' && <Gallery/>}
                    {activeButton === 'Video' && <Video/>}
                    {activeButton === '360 Video' && <Video/>}
                </Grid>
                <Grid item container direction='column' justify='space-between' style={{minHeight: 630, width: '40%'}}>
                    <div className='flex-center-row'>
                        <ColorButton
                            startIcon={<InfoTwoTone/>}
                            variant={activeButton === 'Info' ? "contained" : ""}
                            onClick={() => setActiveButton('Info')}>
                            Info
                        </ColorButton>
                        <div style={{width: 10}}/>
                        <ColorButton
                            startIcon={<PanoramaTwoTone/>}
                            variant={activeButton === 'Photo' ? "contained" : ""}
                            onClick={() => setActiveButton('Photo')}>
                            Photo
                        </ColorButton>
                        <div style={{width: 10}}/>
                        <ColorButton
                            startIcon={<VideocamTwoTone/>}
                            variant={activeButton === 'Video' ? "contained" : ""}
                            onClick={() => setActiveButton('Video')}>
                            Video
                        </ColorButton>
                        <div style={{width: 10}}/>
                        <ColorButton
                            startIcon={<RefreshTwoTone/>}
                            variant={activeButton === '360 Video' ? "contained" : ""}
                            onClick={() => setActiveButton('360 Video')}>
                            360 Video
                        </ColorButton>
                    </div>
                    <img style={{maxWidth: "100%", maxHeight: "100%"}}
                         src="https://kastell.mikado-themes.com/wp-content/uploads/2017/11/map-gallery-img-3.jpg"
                         alt={'image'}/>
                </Grid>
            </Grid>
        </div>
    );
}

export default App;
